package com.redriver.ezstudentmanagementapi.controller;
import com.redriver.ezstudentmanagementapi.exception.ResourceNotFoundException;
import com.redriver.ezstudentmanagementapi.model.MyStudent;
import com.redriver.ezstudentmanagementapi.model.Student;
import com.redriver.ezstudentmanagementapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    // Create api endpoint to get all students
    @GetMapping("/students")
    public List<MyStudent> getAllStudents(){
        return studentRepository.findAll();
    }

    // create a new student
    @PostMapping("/students")
    public MyStudent createStudent(@Valid @RequestBody MyStudent student){
        return  studentRepository.save(student);
    }

    // get a student by id
    @GetMapping("/students/{id}")
    public ResponseEntity<MyStudent> getStudentById( @PathVariable(value = "id") Long studentId) throws ResourceNotFoundException {
        MyStudent student = studentRepository.findById(studentId)
                                            .orElseThrow(() -> new ResourceNotFoundException("Student not found for this id:: " + studentId));
         return ResponseEntity.ok().body(student);
    }

    // update a new student
    @PutMapping("/students/{id}")
    public  ResponseEntity<MyStudent> updateStudent(@PathVariable(value = "id") Long studentId, @RequestBody Student studentDetails) throws ResourceNotFoundException {
        MyStudent student = studentRepository.findById(studentId)
                                           .orElseThrow(() -> new ResourceNotFoundException("Student not found for this id:: " + studentId));
        student.setFirstName(studentDetails.getFirstName());
        student.setLastName(studentDetails.getLastName());
        student.setGpa(studentDetails.getGpa());
        studentRepository.save(student);
        return ResponseEntity.ok().body(student);
    }

    // delete a student by id
    @DeleteMapping("/students/{id}")
    public ResponseEntity.BodyBuilder deleteStudent(@PathVariable(value = "id") Long studentId) throws ResourceNotFoundException {
        studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found for this id:: " + studentId));
        studentRepository.deleteById(studentId);
        return ResponseEntity.ok();
    }
}
