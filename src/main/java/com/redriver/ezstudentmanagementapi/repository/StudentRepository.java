package com.redriver.ezstudentmanagementapi.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.redriver.ezstudentmanagementapi.model.MyStudent;

@Repository
public interface StudentRepository extends JpaRepository<MyStudent, Long> {

}
