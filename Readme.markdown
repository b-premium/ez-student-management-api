# Ez Student Management API

### Readme

1) This application was bootstrapped with Spring Initializr

2) This is the backend for the Red River take home exam.

3) The project is deployed to Heroku for easy testing at the following URL
  
   a) https://ez-student-management-api.herokuapp.com
   
   b) because it's on the free tier you may need to wait a few seconds for the app to wake up.

#### API Endpoints
1) Get All of the students.

   a) https://ez-student-management-api.herokuapp.com/api/v1/students

2) Create a new student (POST)
   
   a) https://ez-student-management-api.herokuapp.com/api/v1/students
   
   b) { "firstName": "james", "lastName": "Thompson", "gpa": "4.0"}



3) Update an existing student (PUT)
   
   a) https://ez-student-management-api.herokuapp.com/api/v1/students/{studentId}
   
   b) { "firstName": "updateFirstName", "lastName": "updateLastName", "gpa": "4.0"}
   
   
3) Delete an existing student (DELETE)
   
   a) https://ez-student-management-api.herokuapp.com/api/v1/students/{studentId}
   
