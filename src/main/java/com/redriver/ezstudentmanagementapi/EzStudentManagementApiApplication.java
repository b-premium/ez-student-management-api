package com.redriver.ezstudentmanagementapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EzStudentManagementApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EzStudentManagementApiApplication.class, args);
		System.out.println("----------Hello red river.");
	}
}
