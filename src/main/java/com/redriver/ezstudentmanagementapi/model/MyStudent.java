package com.redriver.ezstudentmanagementapi.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="MyStudent")
public class MyStudent implements IModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gpa")
    private String gpa;
}
